const fs = require("fs");

const list = [];
const rootDir = __dirname;
const reg = new RegExp(".js$", "");
function registerRouters(path) {
  const files = fs.readdirSync(`${rootDir}${path}`);
  files.forEach((file) => {
    if (reg.test(file) && file !== "routes.js") {
      file = file.replace(/\.js$/, "");
      const obj = {
        path: file === "_index" ? path : `${path}${file}`,
        router: require(`.${path}${file}`),
      };
      list.push(obj);
    } else if (fs.statSync(`${rootDir}${path}${file}`).isDirectory()) {
      registerRouters(`${path}${file}/`);
    }
  });
}
registerRouters("/");
module.exports = list;
